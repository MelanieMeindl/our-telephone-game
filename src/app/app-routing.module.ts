import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomGuard } from './core/guards/room.guard';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '404',
    component: NotFoundComponent,
  },
  {
    path: 'game',
    canActivate: [RoomGuard],
    loadChildren: () =>
      import('./modules/game/game.module').then((m) => m.GameModule),
  },
  {
    path: 'results',
    canActivate: [RoomGuard],
    loadChildren: () =>
      import('./modules/results/results.module').then((m) => m.ResultsModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('./modules/lobby/lobby.module').then((m) => m.LobbyModule),
  },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
