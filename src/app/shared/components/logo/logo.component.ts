import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ClearRoom } from 'src/app/store/room.actions';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent implements OnInit {
  @Input() isCentered = false;

  constructor(private store: Store) {}

  ngOnInit(): void {}

  clearRoomData(): void {
    this.store.dispatch(new ClearRoom());
  }
}
