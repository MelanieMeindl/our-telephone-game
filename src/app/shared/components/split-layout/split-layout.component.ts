import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-split-layout',
  templateUrl: './split-layout.component.html',
  styleUrls: ['./split-layout.component.scss'],
})
export class SplitLayoutComponent implements OnInit {
  @Input() hasCenteredLogo = false;

  constructor(private store: Store) {}

  ngOnInit(): void {}
}
