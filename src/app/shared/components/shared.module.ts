import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LogoComponent } from './logo/logo.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NotificationComponent } from './notification/notification.component';
import { PlayerListComponent } from './player-list/player-list.component';
import { SplitLayoutComponent } from './split-layout/split-layout.component';

@NgModule({
  declarations: [
    PlayerListComponent,
    NotFoundComponent,
    SplitLayoutComponent,
    NotificationComponent,
    LogoComponent,
  ],
  imports: [CommonModule, FormsModule, RouterModule],
  exports: [
    PlayerListComponent,
    NotFoundComponent,
    SplitLayoutComponent,
    NotificationComponent,
    LogoComponent,
  ],
})
export class SharedModule {}
