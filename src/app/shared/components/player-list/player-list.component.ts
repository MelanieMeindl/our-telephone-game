import { Component, Input, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Client } from 'src/app/core/interfaces/client.interface';
import { ClientState } from './../../../store/clients.state';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss'],
})
export class PlayerListComponent implements OnInit {
  @Select(ClientState.clients) clients$: Observable<Client[]>;
  @Input() highlightedPlayer: number;

  constructor() {}

  ngOnInit(): void {}

  trackById(index: number, client: Client): string {
    return client.id;
  }
}
