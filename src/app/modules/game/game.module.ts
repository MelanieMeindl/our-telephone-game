import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { SharedModule } from 'src/app/shared/components/shared.module';
import { DrawingEditorComponent } from './components/drawing-editor/drawing-editor.component';
import { DrawingComponent } from './components/drawing/drawing.component';
import { GameComponent } from './components/game/game.component';
import { StoryComponent } from './components/story/story.component';
import { TimerComponent } from './components/timer/timer.component';
import { GameRoutingModule } from './game-routing.module';

@NgModule({
  declarations: [
    GameComponent,
    DrawingEditorComponent,
    StoryComponent,
    DrawingComponent,
    TimerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GameRoutingModule,
    SharedModule,
    NgCircleProgressModule.forRoot(),
  ],
})
export class GameModule {}
