import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlbumEntry } from 'src/app/core/interfaces/album-entry.interface';
import { AlbumService } from 'src/app/core/services/album.service';
import { GameManagerService } from 'src/app/core/services/game-manager.service';

@Component({
  selector: 'app-drawing',
  templateUrl: './drawing.component.html',
  styleUrls: ['./drawing.component.scss'],
})
export class DrawingComponent implements OnInit, OnDestroy {
  public emittedImageString = '';
  public nextClientId: string;
  public albumEntry: AlbumEntry;
  public isDone = false;

  private readonly duration = 50000;

  constructor(
    private albumService: AlbumService,
    public gameManagerService: GameManagerService
  ) {}

  ngOnInit(): void {
    this.nextClientId = this.gameManagerService.getNextClient();

    this.albumService
      .getLastAlbumEntryFromClientObservable(this.nextClientId)
      .subscribe((data) => (this.albumEntry = data.pop()));

    this.gameManagerService.roundTimer(this.duration);
  }

  ngOnDestroy(): void {
    if (!this.gameManagerService.clientHasLeft) {
      this.addAlbumEntry();
      clearInterval(this.gameManagerService.interval);
    }
  }

  addAlbumEntry(): void {
    this.albumService.addAlbumEntry(this.emittedImageString, this.nextClientId);
  }

  updateDrawing(image: string): void {
    this.emittedImageString = image;
  }

  public done(): void {
    this.gameManagerService.clientDone();
  }

  public edit(): void {
    this.gameManagerService.clientUndone();
  }
}
