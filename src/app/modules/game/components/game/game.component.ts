import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { ActivePage, Room } from 'src/app/core/interfaces/room.interface';
import { GameManagerService } from 'src/app/core/services/game-manager.service';
import { RoomState } from 'src/app/store/room.state';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, OnDestroy {
  @Select(RoomState.room) room$: Observable<Room>;

  private roomSubscription: Subscription;
  public currentRound: number;

  constructor(
    private gameManagerService: GameManagerService,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.roomSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.roomSubscription = this.room$.subscribe((room) => {
      if (room) {
        if (room.currentPage === ActivePage.RESULTS) {
          this.router.navigate([ActivePage.RESULTS]);
        }
        if (
          room.clientsDone >=
            this.gameManagerService.clients.length -
              Number(this.gameManagerService.room.clientsLeft) &&
          this.gameManagerService.client.isHost
        ) {
          this.gameManagerService.nextRound();
        }
        this.currentRound = room.currentRound;
      }
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  doSomething(): any {
    this.gameManagerService.clientHasLeft = true;
    this.gameManagerService.clientLeft();
    this.gameManagerService.addEntriesFromLeavingClient();
  }
}
