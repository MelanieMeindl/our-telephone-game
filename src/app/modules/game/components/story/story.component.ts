import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlbumEntry } from 'src/app/core/interfaces/album-entry.interface';
import { AlbumService } from 'src/app/core/services/album.service';
import { GameManagerService } from 'src/app/core/services/game-manager.service';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss'],
})
export class StoryComponent implements OnInit, OnDestroy {
  public story = '';
  public nextClientId: string;
  public albumEntry: AlbumEntry;
  public isDone = false;

  private readonly duration = 30000;

  constructor(
    private albumService: AlbumService,
    public gameManagerService: GameManagerService
  ) {}

  ngOnInit(): void {
    this.nextClientId = this.gameManagerService.getNextClient();
    if (this.gameManagerService.room.currentRound !== 0) {
      this.albumService
        .getLastAlbumEntryFromClientObservable(this.nextClientId)
        .subscribe((data) => (this.albumEntry = data.pop()));
    }
    this.gameManagerService.roundTimer(this.duration);
  }

  ngOnDestroy(): void {
    if (!this.gameManagerService.clientHasLeft) {
      this.addAlbumEntry();
      clearInterval(this.gameManagerService.interval);
    }
  }

  addAlbumEntry(): void {
    this.albumService.addAlbumEntry(this.story, this.nextClientId);
  }

  done(): void {
    this.isDone = true;
    this.gameManagerService.clientDone();
  }

  edit(): void {
    this.isDone = false;
    this.gameManagerService.clientUndone();
  }
}
