import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, pairwise, switchMap, takeUntil } from 'rxjs/operators';
import { GameManagerService } from 'src/app/core/services/game-manager.service';

@Component({
  selector: 'app-drawing-editor',
  templateUrl: './drawing-editor.component.html',
  styleUrls: ['./drawing-editor.component.scss'],
})
export class DrawingEditorComponent implements AfterViewInit, OnDestroy {
  @ViewChild('canvas') public canvasRef!: ElementRef;
  @Output() drawingChanged = new EventEmitter<string>();
  @Output() drawingDone = new EventEmitter<boolean>();
  @Output() editDrawing = new EventEmitter<boolean>();
  public width = 800;
  public height = 400;
  public isDone = false;

  public colors: string[] = [
    '#000000',
    '#FF0000',
    '#d81b60',
    '#00FF00',
    '#3f974f',
    '#0000FF',
    '#FFFF00',
    '#8e2da8',
    '#1b4076',
  ];
  public selectedColor: string = this.colors[0];

  private ctx!: CanvasRenderingContext2D;
  private drawSubscription!: Subscription;
  private emitDrawingSubscription!: Subscription;
  private lineCap: CanvasLineCap = 'round';
  private lineWidth = 4;
  private timeout = 500;
  private drawnLines: Line[] = [];

  get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  constructor(public gameManagerService: GameManagerService) {}

  ngAfterViewInit(): void {
    this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;
    this.calculateCanvasSize(window.innerWidth);

    if (this.ctx) {
      this.ctx.lineWidth = this.lineWidth;
      this.ctx.lineCap = this.lineCap;

      this.detectDrawingOnCanvas();
      this.emitDrawingAfterMouseUp();
    }
  }

  ngOnDestroy(): void {
    this.drawSubscription.unsubscribe();
    this.emitDrawingSubscription.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.redrawCanvasAfterResize(event.target.innerWidth);
  }

  /**
   * Capture all mousedown events, record the mouse movements and subscribe to it
   */
  private detectDrawingOnCanvas(): void {
    this.drawSubscription = fromEvent(this.canvas, 'mousedown')
      .pipe(
        switchMap(() => {
          return fromEvent(this.canvas, 'mousemove').pipe(
            // stop once the mouseup event is triggered
            takeUntil(fromEvent(this.canvas, 'mouseup')),
            // stop once the mouseleave event is triggered
            takeUntil(fromEvent(this.canvas, 'mouseleave')),
            pairwise()
          );
        })
      )
      .subscribe((res) => {
        const rect = this.canvas.getBoundingClientRect();
        const mouseEvent = res as [MouseEvent, MouseEvent];

        const previousPosition = {
          x: mouseEvent[0].clientX - rect.left,
          y: mouseEvent[0].clientY - rect.top,
        };

        const currentPosition = {
          x: mouseEvent[1].clientX - rect.left,
          y: mouseEvent[1].clientY - rect.top,
        };

        this.drawOnCanvas(
          previousPosition,
          currentPosition,
          this.selectedColor
        );

        this.drawnLines.push({
          fromPos: previousPosition,
          toPos: currentPosition,
          color: this.selectedColor,
        });
      });
  }

  /**
   * Does the actual drawing on the canvas
   */
  private drawOnCanvas(
    previousPosition: Point,
    currentPosition: Point,
    color: string
  ): void {
    this.ctx.strokeStyle = color;
    this.ctx.beginPath();

    this.ctx.moveTo(previousPosition.x, previousPosition.y);
    this.ctx.lineTo(currentPosition.x, currentPosition.y);
    this.ctx.stroke();
  }

  /**
   * Emits drawing as base64 string
   */
  private emitDrawingAfterMouseUp(): void {
    this.emitDrawingSubscription = fromEvent(this.canvas, 'mouseup')
      .pipe(debounceTime(this.timeout))
      .subscribe(() => this.drawingChanged.emit(this.canvas.toDataURL()));
  }

  /**
   * Calculate canvas size based on window width
   */
  private calculateCanvasSize(windowWidth: number): void {
    this.width = windowWidth / 1.7;
    this.height = this.width * 0.5;
    this.canvas.width = this.width;
    this.canvas.height = this.height;
  }

  /**
   * Redraw and resize canvas for a certain window width
   */
  private redrawCanvasAfterResize(windowWidth: number): void {
    const oldWindowWidth = this.canvas.width;
    this.calculateCanvasSize(windowWidth);
    const scaleFactor = this.canvas.width / oldWindowWidth;
    this.ctx.lineWidth = this.lineWidth;

    this.drawnLines.forEach((line) => {
      line.fromPos.x *= scaleFactor;
      line.fromPos.y *= scaleFactor;
      line.toPos.x *= scaleFactor;
      line.toPos.y *= scaleFactor;
      this.drawOnCanvas(line.fromPos, line.toPos, line.color);
    });
  }

  /**
   * Clear the whole canvas
   */
  public clearCanvas(): void {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.drawingChanged.emit(this.canvas.toDataURL());
  }

  public emitDone(): void {
    this.isDone = true;
    this.drawingDone.emit(this.isDone);
  }

  public emitEdit(): void {
    this.isDone = false;
    this.editDrawing.emit(this.isDone);
  }
}

interface Point {
  x: number;
  y: number;
}

interface Line {
  fromPos: Point;
  toPos: Point;
  color: string;
}
