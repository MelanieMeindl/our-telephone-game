import { Component, OnInit } from '@angular/core';
import { GameManagerService } from 'src/app/core/services/game-manager.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
})
export class TimerComponent implements OnInit {
  constructor(public gameManagerService: GameManagerService) {}

  math = Math;

  ngOnInit(): void {}
}
