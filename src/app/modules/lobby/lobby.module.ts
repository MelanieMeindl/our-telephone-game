import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/components/shared.module';
import { AvatarComponent } from './components/avatar/avatar.component';
import { HomeComponent } from './components/home/home.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { LobbyRoutingModule } from './lobby-routing.module';

@NgModule({
  declarations: [LobbyComponent, AvatarComponent, HomeComponent],
  imports: [CommonModule, LobbyRoutingModule, FormsModule, SharedModule],
})
export class LobbyModule {}
