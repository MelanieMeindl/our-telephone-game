import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { Client } from 'src/app/core/interfaces/client.interface';
import { ActivePage, Room } from 'src/app/core/interfaces/room.interface';
import { ClientState } from 'src/app/store/clients.state';
import { UpdateRoom } from 'src/app/store/room.actions';
import { NotificationService } from './../../../../core/services/notification.service';
import { RoomState } from './../../../../store/room.state';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss'],
})
export class LobbyComponent implements OnInit, OnDestroy {
  @Select(RoomState.room) room$: Observable<Room>;
  public client: Client = null;

  private roomSubscription: Subscription;
  private clientSubscription: Subscription;

  constructor(
    private store: Store,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  ngOnDestroy(): void {
    this.roomSubscription.unsubscribe();
    this.clientSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.monitorPageChange();
    this.getCurrentUser();
  }

  /**
   * Copy the link to join the game to the clipboard
   */
  copyLinkToClipboard(): void {
    const roomId = this.store.selectSnapshot(RoomState.room).id;
    navigator.clipboard.writeText('localhost:4200/' + roomId).then(
      () =>
        this.notificationService.show('Link successfully copied to clipboard!'),
      () =>
        this.notificationService.show(
          'Oups! Link could not be copied to clipboard!',
          'error'
        )
    );
  }

  /**
   * Start the game
   */
  startGame(): void {
    const room = this.store.selectSnapshot(RoomState.room);
    if (room) {
      room.currentPage = ActivePage.GAME;
      room.timestamp = +new Date();
      this.store.dispatch(new UpdateRoom(room));
    }
  }

  /**
   * Subscribe to room and check if currentPage is changed to game
   */
  private monitorPageChange(): void {
    this.roomSubscription = this.room$.subscribe((room) => {
      if (room && room.currentPage === ActivePage.GAME) {
        this.router.navigate([ActivePage.GAME]);
      }
    });
  }

  /**
   * Subscribe to clients and get corresponding client
   */
  private getCurrentUser(): void {
    this.clientSubscription = this.store
      .select(ClientState.client)
      .subscribe((client) => {
        this.client = client;
      });
  }
}
