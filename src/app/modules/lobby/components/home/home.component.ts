import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { Client } from 'src/app/core/interfaces/client.interface';
import { ActivePage, Room } from 'src/app/core/interfaces/room.interface';
import { AuthState } from 'src/app/store/auth.state';
import { AddClient } from 'src/app/store/clients.actions';
import { CreateRoom, GetRoom } from './../../../../store/room.actions';
import { RoomState } from './../../../../store/room.state';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  @Select(RoomState.room) room$: Observable<Room>;
  public nickname = '';
  public roomId: string | null = null;
  public avatarImages: string[] = [];

  private avatar = '';
  private roomSubscription: Subscription;
  private addClientSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store,
    private router: Router
  ) {}

  ngOnDestroy(): void {
    this.roomSubscription?.unsubscribe();
    this.addClientSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.roomId = this.activatedRoute.snapshot.paramMap.get('roomID');
    if (this.roomId) {
      this.store.dispatch(new GetRoom(this.roomId));
    } else {
      this.roomSubscription = this.room$.subscribe((room) => {
        if (room) {
          this.goToLobby();
        }
      });
    }
  }

  /**
   * Save the current avatar if it is changed in the avatar component
   */
  setAvatar(image: string): void {
    this.avatar = image;
  }

  /**
   * Start a new game or join an existing one with a given room id
   */
  async startOrJoinGame(): Promise<void> {
    if (this.roomId) {
      const client: Client = {
        id: this.store.selectSnapshot(AuthState.userId),
        image: this.avatar,
        isHost: false,
        name: this.nickname,
        joinedAt: +new Date(),
      };

      this.addClientSubscription = this.store
        .dispatch(new AddClient(this.roomId, client))
        .subscribe(() => {
          this.goToLobby();
        });
    } else {
      const newRoom: Room = {
        currentPage: ActivePage.LOBBY,
        clientsDone: 0,
        currentRound: 0,
        timestamp: 0,
        clientsLeft: 0,
      };

      const client: Client = {
        id: this.store.selectSnapshot(AuthState.userId),
        image: this.avatar,
        isHost: true,
        name: this.nickname,
        joinedAt: +new Date(),
      };

      this.store.dispatch(new CreateRoom(newRoom, client));
    }
  }

  private goToLobby(): void {
    this.router.navigate(['lobby']);
  }
}
