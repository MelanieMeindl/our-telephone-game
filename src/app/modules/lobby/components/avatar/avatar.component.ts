import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Subscription } from 'rxjs';
import { map, mergeAll } from 'rxjs/operators';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit, OnDestroy {
  @Output() avatarChanged = new EventEmitter<string>();
  public avatars: string[] = [];
  public currentAvatar = '';

  private avatarSubscription: Subscription;

  constructor(private fireStorage: AngularFireStorage) {}

  ngOnDestroy(): void {
    this.avatarSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.retreiveAllAvatars();
  }

  /**
   * Retreive all avatar images stored in the firestore storage
   */
  private async retreiveAllAvatars(): Promise<void> {
    this.avatarSubscription = this.fireStorage
      .ref('images')
      .listAll()
      .pipe(
        map(
          async (images) =>
            await Promise.all(images.items.map((item) => item.getDownloadURL()))
        ),
        mergeAll()
      )
      .subscribe((avatars: string[]) => {
        this.avatars = avatars;
        this.getRandomAvatar();
      });
  }

  /**
   * Set the current avatar by generating a random index
   */
  getRandomAvatar(): void {
    const randomIndex = Math.floor(Math.random() * this.avatars.length);
    if (this.currentAvatar === this.avatars[randomIndex]) {
      this.getRandomAvatar();
    } else {
      this.currentAvatar = this.avatars[randomIndex];
      this.avatarChanged.emit(this.currentAvatar);
    }
  }
}
