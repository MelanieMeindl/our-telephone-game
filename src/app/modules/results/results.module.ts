import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/components/shared.module';
import { ClientLabelComponent } from './components/client-label/client-label.component';
import { DrawingResultComponent } from './components/drawing-result/drawing-result.component';
import { ResultsComponent } from './components/results/results.component';
import { StoryResultComponent } from './components/story-result/story-result.component';
import { ResultsRoutingModule } from './results-routing.module';

@NgModule({
  declarations: [
    ResultsComponent,
    DrawingResultComponent,
    StoryResultComponent,
    ClientLabelComponent,
  ],
  imports: [CommonModule, ResultsRoutingModule, SharedModule, CoreModule],
})
export class ResultsModule {}
