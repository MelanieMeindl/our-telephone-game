import { Component, Input, OnInit } from '@angular/core';
import { AlbumEntry } from 'src/app/core/interfaces/album-entry.interface';

@Component({
  selector: 'app-story-result',
  templateUrl: './story-result.component.html',
  styleUrls: ['./story-result.component.scss'],
})
export class StoryResultComponent implements OnInit {
  @Input() albumEntry: AlbumEntry;

  constructor() {}

  ngOnInit(): void {}
}
