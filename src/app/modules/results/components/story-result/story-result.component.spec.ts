import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryResultComponent } from './story-result.component';

describe('StoryResultComponent', () => {
  let component: StoryResultComponent;
  let fixture: ComponentFixture<StoryResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoryResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
