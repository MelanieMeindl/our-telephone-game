import { Component, Input, OnInit } from '@angular/core';
import { Client } from 'src/app/core/interfaces/client.interface';

@Component({
  selector: 'app-client-label',
  templateUrl: './client-label.component.html',
  styleUrls: ['./client-label.component.scss'],
})
export class ClientLabelComponent implements OnInit {
  @Input() client: Client;
  @Input() isRight = true;

  constructor() {}

  ngOnInit(): void {}
}
