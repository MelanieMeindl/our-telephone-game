import { Component, Input, OnInit } from '@angular/core';
import { AlbumEntry } from 'src/app/core/interfaces/album-entry.interface';

@Component({
  selector: 'app-drawing-result',
  templateUrl: './drawing-result.component.html',
  styleUrls: ['./drawing-result.component.scss'],
})
export class DrawingResultComponent implements OnInit {
  @Input() albumEntry: AlbumEntry;

  constructor() {}

  ngOnInit(): void {}
}
