import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingResultComponent } from './drawing-result.component';

describe('DrawingResultComponent', () => {
  let component: DrawingResultComponent;
  let fixture: ComponentFixture<DrawingResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrawingResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
