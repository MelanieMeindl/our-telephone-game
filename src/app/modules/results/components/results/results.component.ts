import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { from, interval, Observable, of, Subscription } from 'rxjs';
import { concatMap, debounceTime, delay } from 'rxjs/operators';
import { Client } from 'src/app/core/interfaces/client.interface';
import { Room } from 'src/app/core/interfaces/room.interface';
import { AlbumService } from 'src/app/core/services/album.service';
import { ClientState } from 'src/app/store/clients.state';
import { UpdateRoom } from 'src/app/store/room.actions';
import { RoomState } from 'src/app/store/room.state';
import { AlbumEntry } from './../../../../core/interfaces/album-entry.interface';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit, OnDestroy {
  @Select(ClientState.clients) clients$: Observable<Client[]>;
  @Select(RoomState.room) room$: Observable<Room>;
  public currentAlbum: AlbumEntry[] = null;
  public albumEntriesToShow: AlbumEntry[] = null;
  public currentClientIndex: number = null;
  public clients: Client[];
  public client: Client;
  public room: Room;
  public interval: Observable<number> = interval(1000);

  private clientSubscription: Subscription;
  private roomSubscription: Subscription;
  private albumSubscription: Subscription;
  private readonly delay = 1500;

  constructor(private store: Store, private albumService: AlbumService) {}

  ngOnInit(): void {
    this.clientSubscription = this.clients$.subscribe((clients) => {
      if (clients) {
        this.clients = clients;
        this.client = this.store.selectSnapshot(ClientState.client);
      }
    });

    this.roomSubscription = this.room$
      .pipe(debounceTime(500))
      .subscribe(async (room) => {
        if (room) {
          this.room = room;
          if (room.albumClientId) {
            this.handleAlbumClientIdChanged(room.albumClientId);
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.clientSubscription.unsubscribe();
    this.roomSubscription.unsubscribe();
    this.albumSubscription?.unsubscribe();
  }

  trackById(index: number, albumEntry: AlbumEntry): string {
    return albumEntry.id;
  }

  showNextAlbum(): void {
    this.currentClientIndex =
      this.currentClientIndex !== null ? ++this.currentClientIndex : 0;
    this.updateAlbumInDB(this.currentClientIndex);
  }

  showPreviousAlbum(): void {
    this.updateAlbumInDB(--this.currentClientIndex);
  }

  private updateAlbumInDB(index: number): void {
    this.room.albumClientId = this.clients[index].id;
    this.store.dispatch(new UpdateRoom(this.room));
  }

  /**
   * Show the album entries sequentially with a specified delay
   */
  private showAlbumEntries(): void {
    this.albumSubscription = from(this.currentAlbum)
      .pipe(concatMap((entry) => of(entry).pipe(delay(this.delay))))
      .subscribe((entry) => this.albumEntriesToShow.push(entry));
  }

  /**
   * All actions that need to be carried out for showing a new album
   */
  private async handleAlbumClientIdChanged(clientId: string): Promise<void> {
    this.albumSubscription?.unsubscribe();
    this.albumEntriesToShow = [];
    this.currentAlbum = await this.albumService.getAllAlbumEntriesFromClient(
      clientId
    );
    if (!this.client.isHost) {
      this.currentClientIndex = this.clients.findIndex(
        (client) => client.id === clientId
      );
    }
    this.showAlbumEntries();
  }
}
