import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  Action,
  NgxsOnInit,
  Selector,
  State,
  StateContext,
  Store,
} from '@ngxs/store';
import { of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Client } from '../core/interfaces/client.interface';
import { DatabaseService } from '../core/services/database.service';
import { NotificationService } from './../core/services/notification.service';
import { AuthState, AuthStateModel } from './auth.state';
import { AddClient, SetClients } from './clients.actions';
import { RoomState } from './room.state';

export interface ClientStateModel {
  clients: Client[];
}

@State<ClientStateModel>({
  name: 'clientState',
})
@Injectable()
export class ClientState implements NgxsOnInit {
  @Selector()
  static clients(state: ClientStateModel): Client[] {
    return state.clients;
  }

  @Selector([AuthState])
  static client(state: ClientStateModel, authState: AuthStateModel): Client {
    return state.clients.find((client) => client.id === authState.user.uid);
  }

  constructor(
    private store: Store,
    private router: Router,
    private notificationService: NotificationService,
    private databaseService: DatabaseService
  ) {}

  ngxsOnInit(context?: StateContext<any>): void {
    this.store
      .select(RoomState.roomId)
      .pipe(
        switchMap((roomId) => {
          if (!roomId) {
            return of(null);
          } else {
            return this.databaseService
              .getClients(roomId)
              .valueChanges({
                idField: 'id',
              })
              .pipe(
                tap((clients) => {
                  context?.dispatch(new SetClients(clients));
                })
              );
          }
        })
      )
      .subscribe();
  }

  @Action(AddClient)
  addClient(context: StateContext<ClientStateModel>, action: AddClient): void {
    this.databaseService
      .getClient(action.roomId, action.client.id)
      .get()
      .subscribe((client) => {
        if (!client.exists) {
          this.databaseService
            .getClientPartial(action.roomId, action.client.id)
            .set({
              image: action.client.image,
              isHost: action.client.isHost,
              name: action.client.name,
              joinedAt: action.client.joinedAt,
            });
        } else {
          this.notificationService.show(
            'Oups! You are already present in the room.',
            'error'
          );
          this.router.navigate(['']);
        }
      });
  }

  @Action(SetClients)
  setClients(
    context: StateContext<ClientStateModel>,
    action: SetClients
  ): void {
    action.clients.sort((client1, client2) => {
      return client1.joinedAt - client2.joinedAt;
    });
    context.patchState({
      clients: action.clients,
    });
  }
}
