import { Client } from '../core/interfaces/client.interface';
import { Room } from '../core/interfaces/room.interface';

export class CreateRoom {
  static readonly type = '[Room] CreateRoom';

  constructor(public room: Room, public client: Client) {}
}

export class GetRoom {
  static readonly type = '[Room] GetRoom';

  constructor(public roomId: string) {}
}

export class SetRoom {
  static readonly type = '[Room] SetRoom';

  constructor(public room: Room) {}
}

export class UpdateRoom {
  static readonly type = '[Room] UpdateRoomData';

  constructor(public room: Room) {}
}

export class IncrementClients {
  static readonly type = '[Room] IncrementClients';

  constructor(public roomId: string) {}
}

export class IncrementClientsLeft {
  static readonly type = '[Room] IncrementClientsLeft';

  constructor(public roomId: string) {}
}

export class DecrementClients {
  static readonly type = '[Room] DecrementClients';

  constructor(public roomId: string) {}
}

export class ClearRoom {
  static readonly type = '[Room] ClearRoom';

  constructor() {}
}
