import { Client } from '../core/interfaces/client.interface';

export class AddClient {
  static readonly type = '[Client] AddClient';

  constructor(public roomId: string, public client: Client) {}
}

export class SetClients {
  static readonly type = '[Client] SetClients';

  constructor(public clients: Client[]) {}
}
