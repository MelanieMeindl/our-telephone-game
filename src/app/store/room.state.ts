import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  Action,
  NgxsOnInit,
  Selector,
  State,
  StateContext,
  Store,
} from '@ngxs/store';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { Room } from '../core/interfaces/room.interface';
import { DatabaseService } from '../core/services/database.service';
import { NotificationService } from './../core/services/notification.service';
import { AddClient } from './clients.actions';
import {
  ClearRoom,
  CreateRoom,
  DecrementClients,
  GetRoom,
  IncrementClients,
  IncrementClientsLeft,
  SetRoom,
  UpdateRoom,
} from './room.actions';

export interface RoomStateModel {
  room: Room;
}

@State<RoomStateModel>({
  name: 'roomState',
})
@Injectable()
export class RoomState implements NgxsOnInit {
  @Selector()
  static room(state: RoomStateModel): Room {
    return state.room || null;
  }

  @Selector()
  static roomId(state: RoomStateModel): string | null {
    return state.room.id || null;
  }

  constructor(
    private store: Store,
    private router: Router,
    private notificationService: NotificationService,
    private databaseService: DatabaseService
  ) {}

  ngxsOnInit(context?: StateContext<any>): void {}

  @Action(CreateRoom)
  async createRoom(
    context: StateContext<RoomStateModel>,
    action: CreateRoom
  ): Promise<void> {
    const ref = await this.databaseService.getRooms().add(action.room);
    await this.store.dispatch(new GetRoom(ref.id)).toPromise();
    await this.store.dispatch(new AddClient(ref.id, action.client)).toPromise();
  }

  @Action(GetRoom)
  getRoom(context: StateContext<RoomStateModel>, action: GetRoom): void {
    const roomRef = this.databaseService.getRoom(action.roomId);

    roomRef.get().subscribe((room) => {
      if (room.exists) {
        roomRef
          .valueChanges({
            idField: 'id',
          })
          .subscribe(async (newRoom) => {
            await this.store.dispatch(new SetRoom(newRoom)).toPromise();
          });
      } else {
        this.notificationService.show(
          'Oups! This room does not exist.',
          'error'
        );
        this.router.navigate(['']);
      }
    });
  }

  @Action(SetRoom)
  setRoom(context: StateContext<RoomStateModel>, action: SetRoom): void {
    context.patchState({
      room: action.room,
    });
  }

  @Action(UpdateRoom)
  updateRoom(context: StateContext<RoomStateModel>, action: UpdateRoom): void {
    this.databaseService
      .getRoom(action.room.id)
      .update(action.room)
      .catch(() => {
        this.notificationService.show(
          'Oups! Failed to Update the room',
          'error'
        );
      });
  }

  @Action(IncrementClients)
  incrementClients(
    context: StateContext<RoomStateModel>,
    action: IncrementClients
  ): void {
    this.databaseService
      .getRoom(action.roomId)
      .update({
        clientsDone: firebase.default.firestore.FieldValue.increment(1),
      })
      .catch(() => {
        this.notificationService.show(
          'Oups! Error occured while setting your masterpiece to done - please click done again or wait until the timer expires',
          'error'
        );
      });
  }

  @Action(IncrementClientsLeft)
  incrementClientsLeft(
    context: StateContext<RoomStateModel>,
    action: IncrementClientsLeft
  ): void {
    this.databaseService
      .getRoom(action.roomId)
      .update({
        clientsLeft: firebase.default.firestore.FieldValue.increment(1),
      })
      .catch(() => {
        this.notificationService.show(
          'Oups! Error occured while setting your masterpiece to done - please click done again or wait until the timer expires',
          'error'
        );
      });
  }

  @Action(DecrementClients)
  decrementClients(
    context: StateContext<RoomStateModel>,
    action: DecrementClients
  ): void {
    this.databaseService
      .getRoom(action.roomId)
      .update({
        clientsDone: firebase.default.firestore.FieldValue.increment(-1),
      })
      .catch(() => {
        this.notificationService.show(
          'Oups! Error occured while setting your masterpiece to done - please click done again or wait until the timer expires',
          'error'
        );
      });
  }

  @Action(ClearRoom)
  clearRoom(context: StateContext<RoomStateModel>, action: ClearRoom): void {
    context.setState({
      room: null,
    });
  }
}
