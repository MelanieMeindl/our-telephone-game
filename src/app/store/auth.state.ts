import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import firebase from 'firebase';
import { UserChanged } from './auth.actions';
import User = firebase.User;

export interface AuthStateModel {
  user: User | null;
}

@State<AuthStateModel>({
  name: 'authState',
  defaults: {
    user: null,
  },
})
@Injectable()
export class AuthState implements NgxsOnInit {
  constructor(private authService: AngularFireAuth) {}

  @Selector()
  static userId(state: AuthStateModel): string | null {
    return state.user?.uid || null;
  }

  ngxsOnInit(context?: StateContext<AuthStateModel>): any {
    this.authService.authState.subscribe((user) => {
      context?.dispatch(new UserChanged(user));
    });
  }

  @Action(UserChanged)
  userChanged(
    context: StateContext<AuthStateModel>,
    action: UserChanged
  ): void {
    context.patchState({
      user: action.user,
    });
  }
}
