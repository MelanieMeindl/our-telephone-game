import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RoomState } from 'src/app/store/room.state';
import { NotificationService } from './../services/notification.service';

@Injectable({
  providedIn: 'root',
})
export class RoomGuard implements CanActivate {
  constructor(
    private store: Store,
    private router: Router,
    private notificationService: NotificationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.store.selectSnapshot(RoomState.room)) {
      this.router.navigate(['/']);
      this.notificationService.show(
        'You are not authorized to access this page. Go ahead and start or join a game.',
        'error'
      );
      return false;
    }
    return true;
  }
}
