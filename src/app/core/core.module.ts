import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DebounceClickDirective } from './directives/debounce-click.directive';

@NgModule({
  declarations: [DebounceClickDirective],
  imports: [CommonModule],
  exports: [DebounceClickDirective],
})
export class CoreModule {}
