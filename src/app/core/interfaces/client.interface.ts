export interface Client {
  id: string;
  name: string;
  image: string;
  isHost: boolean;
  joinedAt: number;
}
