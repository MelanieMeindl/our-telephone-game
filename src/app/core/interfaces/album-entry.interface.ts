import { Client } from './client.interface';

export interface AlbumEntry {
  id?: string;
  client: Client;
  content: string;
  createdAt: number;
  round: number;
}
