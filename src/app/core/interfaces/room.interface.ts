export interface Room {
  id?: string;
  currentPage: ActivePage;
  clientsDone: number | firebase.default.firestore.FieldValue;
  currentRound: number;
  timestamp?: number;
  albumClientId?: string;
  clientsLeft: number | firebase.default.firestore.FieldValue;
}

export enum ActivePage {
  LOBBY = 'lobby',
  GAME = 'game',
  RESULTS = 'results',
}
