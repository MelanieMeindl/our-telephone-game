import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { AlbumEntry } from '../interfaces/album-entry.interface';
import { Client } from '../interfaces/client.interface';
import { Room } from '../interfaces/room.interface';

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  constructor(private angularFirestore: AngularFirestore) {}

  getRoom(roomId: string): AngularFirestoreDocument<Room> {
    return this.angularFirestore.collection<Room>('rooms').doc(roomId);
  }

  getRooms(): AngularFirestoreCollection<Room> {
    return this.angularFirestore.collection<Room>('rooms');
  }

  getClient(
    roomId: string,
    clientId: string
  ): AngularFirestoreDocument<Client> {
    return this.getClients(roomId).doc(clientId);
  }

  getClientPartial(
    roomId: string,
    clientId: string
  ): AngularFirestoreDocument<Partial<Client>> {
    return this.getClients(roomId).doc(clientId);
  }

  getClients(roomId: string): AngularFirestoreCollection<Client> {
    return this.getRoom(roomId).collection<Client>('clients');
  }

  getAlbumEntries(
    roomId: string,
    clientId: string
  ): AngularFirestoreCollection<AlbumEntry> {
    return this.getClient(roomId, clientId).collection<AlbumEntry>('album');
  }
}
