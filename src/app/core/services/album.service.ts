import { Injectable, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { GameManagerService } from 'src/app/core/services/game-manager.service';
import { ClientState } from 'src/app/store/clients.state';
import { RoomState } from 'src/app/store/room.state';
import { Client } from '../interfaces/client.interface';
import { Room } from '../interfaces/room.interface';
import { AlbumEntry } from './../interfaces/album-entry.interface';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class AlbumService implements OnDestroy {
  @Select(ClientState.clients) clients$: Observable<Client[]>;
  @Select(RoomState.room) room$: Observable<Room>;

  public room: Room;
  public client: Client;
  private clientSubscription: Subscription;
  private roomSubscription: Subscription;
  private savedAlbums = 0;

  constructor(
    private store: Store,
    private gameManagerService: GameManagerService,
    private databaseService: DatabaseService
  ) {
    this.clientSubscription = this.clients$.subscribe((clients) => {
      if (clients) {
        this.client = this.store.selectSnapshot(ClientState.client);
      }
    });
    this.roomSubscription = this.room$.subscribe((room) => {
      if (room) {
        this.room = room;
      }
    });
  }
  ngOnDestroy(): void {
    this.clientSubscription.unsubscribe();
    this.roomSubscription.unsubscribe();
  }

  /**
   * Add an album entry to the database
   */
  addAlbumEntry(entryContent: string, clientId: string): void {
    const albumEntry: AlbumEntry = {
      client: this.gameManagerService.client,
      content: entryContent,
      createdAt: new Date().getTime(),
      round: this.savedAlbums,
    };

    this.databaseService
      .getAlbumEntries(this.room.id, clientId)
      .add(albumEntry);

    this.savedAlbums = this.savedAlbums + 1;
  }

  /**
   * Gets the last album entry of the specified client and returns an observable
   */
  getLastAlbumEntryFromClientObservable(
    clientId?: string
  ): Observable<AlbumEntry[]> {
    const client = clientId ? clientId : this.client.id;

    return this.databaseService
      .getClient(this.room.id, client)
      .collection<AlbumEntry>('album', (ref) =>
        ref.where('round', '==', this.savedAlbums - 1).limit(1)
      )
      .valueChanges();
  }

  /**
   * Get all album entries from the specified client and return the whole album
   */
  async getAllAlbumEntriesFromClient(clientId: string): Promise<AlbumEntry[]> {
    const result = await this.databaseService
      .getClient(this.room.id, clientId)
      .collection<AlbumEntry>('album', (ref) => ref.orderBy('round', 'asc'))
      .get()
      .toPromise();

    return result.docs.map((entry) => {
      return entry.data();
    });
  }
}
