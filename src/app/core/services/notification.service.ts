import { Injectable } from '@angular/core';
import { timer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  public type: 'success' | 'error' = 'success';
  public isVisible = false;
  public text = '';
  private readonly timeout = 3000;

  constructor() {}

  /**
   * Show the notification and hide it again after the specified timeout
   */
  show(text: string, type: 'success' | 'error' = 'success'): void {
    this.type = type;
    this.text = text;
    this.isVisible = true;

    timer(this.timeout).subscribe(() => {
      this.text = '';
      this.isVisible = false;
    });
  }
}
