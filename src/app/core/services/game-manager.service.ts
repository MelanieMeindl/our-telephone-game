import { Injectable, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { ClientState } from 'src/app/store/clients.state';
import {
  DecrementClients,
  IncrementClients,
  IncrementClientsLeft,
  UpdateRoom,
} from 'src/app/store/room.actions';
import { RoomState } from 'src/app/store/room.state';
import { AlbumEntry } from '../interfaces/album-entry.interface';
import { Client } from '../interfaces/client.interface';
import { ActivePage, Room } from '../interfaces/room.interface';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class GameManagerService implements OnDestroy {
  @Select(ClientState.clients) clients$: Observable<Client[]>;
  @Select(RoomState.room) room$: Observable<Room>;

  private clientSubscription: Subscription = null;
  private roomSubscription: Subscription = null;

  public client: Client;
  public clients: Client[];
  public room: Room;
  public timeLeft: number;
  public interval: any;
  public timerDuration: number;
  public clientHasLeft = false;

  constructor(private store: Store, private databaseService: DatabaseService) {
    this.clientSubscription = this.clients$.subscribe((clients) => {
      if (clients) {
        this.clients = clients;
        this.client = this.store.selectSnapshot(ClientState.client);
      }
    });
    this.roomSubscription = this.room$.subscribe((room) => {
      if (room) {
        this.room = room;
      }
    });
  }

  ngOnDestroy(): void {
    this.clientSubscription.unsubscribe();
    this.roomSubscription.unsubscribe();
  }

  clientDone(): void {
    this.store.dispatch(new IncrementClients(this.room.id));
  }

  clientUndone(): void {
    this.store.dispatch(new DecrementClients(this.room.id));
  }

  clientLeft(): void {
    this.store.dispatch(new IncrementClientsLeft(this.room.id));
  }

  /**
   * Sets Game to the next round, until the final round is reached
   */
  nextRound(): void {
    if (this.room.currentRound + 1 >= this.clients.length) {
      this.finishGame();
    } else {
      this.room.clientsDone = 0;
      this.room.currentRound = this.room.currentRound + 1;
      this.room.timestamp = new Date().getTime();
      this.store.dispatch(new UpdateRoom(this.room));
    }
  }

  /**
   * All actions for completing the game, including changing the active page to results
   */
  finishGame(): void {
    this.room.currentPage = ActivePage.RESULTS;
    this.room.timestamp = new Date().getTime();
    this.room.clientsLeft = 0;
    this.room.clientsDone = 0;
    this.store.dispatch(new UpdateRoom(this.room));
  }

  /**
   * Gets the next client whose album will be used for the round
   */
  getNextClient(): string {
    let index = this.clients.indexOf(this.client) + this.room.currentRound;
    if (index >= this.clients.length) {
      index = index - this.clients.length;
    }
    return this.clients[index].id;
  }

  /**
   * Gets the next client for a leaving client whose album will be used for the next round
   */
  getNextClientForLeave(round: number): string {
    let index = this.clients.indexOf(this.client) + round;
    if (index >= this.clients.length) {
      index = index - this.clients.length;
    }
    return this.clients[index].id;
  }

  /**
   * Creates a timer with a certain duration and checks for it's expiration
   */
  roundTimer(duration: number): void {
    this.timerDuration = duration;
    this.interval = setInterval(() => {
      if (new Date().getTime() - this.room.timestamp >= this.timerDuration) {
        clearInterval(this.interval);
        if (this.client.isHost) {
          this.nextRound();
        }
      } else {
        this.timeLeft = Math.floor(
          this.room.timestamp + this.timerDuration - new Date().getTime()
        );
      }
    }, 100);
  }

  /**
   * If a client leaves before the game ends, empty entries are set for all remaining rounds
   */
  addEntriesFromLeavingClient(): void {
    const roundsLeft = this.clients.length - this.room.currentRound;
    let currentRound = this.room.currentRound;

    for (let i = 0; i < roundsLeft; i++) {
      const albumEntry: AlbumEntry = {
        client: this.client,
        content: '',
        createdAt: new Date().getTime(),
        round: currentRound,
      };

      this.databaseService
        .getAlbumEntries(this.room.id, this.getNextClientForLeave(currentRound))
        .add(albumEntry);

      currentRound++;
    }
  }
}
